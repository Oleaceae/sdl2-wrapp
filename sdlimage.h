#ifndef SDL_IMAGE_WRAPPER_H
#define SDL_IMAGE_WRAPPER_H

#include <string>

#include "sdl.h"

namespace sdlwrapp {

class SDLImage {
public:
  SDLImage();
  ~SDLImage();

  static sdlwrapp::SDLTexture load(const sdlwrapp::SDLRenderer& renderer, const std::string& file);
  static sdlwrapp::SDLSurface load(const std::string& file);
};

} // namespace sdlwrapp

#endif
