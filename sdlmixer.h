#ifndef SDL_MIXER_WRAPPER_H
#define SDL_MIXER_WRAPPER_H

#include <SDL2/SDL_mixer.h>
#include <string>

namespace sdlwrapp {

class SDLMixer {
public:
  SDLMixer();
  ~SDLMixer();

  static void setChannelCount(int channels) noexcept;
  static void setChannelFinishedCallback(void(SDLCALL* channelFinished)(int channel)) noexcept;
};

class MixChunk {
public:
  explicit MixChunk(const std::string& filePath);
  ~MixChunk();
  MixChunk(const MixChunk& other) = delete;
  MixChunk(MixChunk&& other) noexcept;

  MixChunk& operator=(const MixChunk& other) = delete;
  MixChunk& operator=(MixChunk&& other) noexcept;

  [[nodiscard]] int play(int channel = -1, int loops = 0) const;

  [[nodiscard]] bool isNull() const noexcept;

private:
  Mix_Chunk* chunk{nullptr};
};

class MixMusic {
public:
  explicit MixMusic(const std::string& filePath);
  ~MixMusic();
  MixMusic(const MixMusic& other) = delete;
  MixMusic(MixMusic&& other) noexcept;

  MixMusic& operator=(const MixMusic& other) = delete;
  MixMusic& operator=(MixMusic&& other) noexcept;

  [[nodiscard]] int play(int loops = 0) const;
  static void pause();
  static void resume();
  static void halt();
  static int setVolume(int volume);

  [[nodiscard]] bool isNull() const noexcept;

private:
  Mix_Music* music{nullptr};
};

} // namespace sdlwrapp

#endif
