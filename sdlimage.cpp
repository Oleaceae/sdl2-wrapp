#include "sdlimage.h"

#include <SDL2/SDL_image.h>

using namespace sdlwrapp;

SDLImage::SDLImage() {
  if (IMG_Init(IMG_INIT_PNG) < 0) {
    throw SDLException();
  }
}

SDLImage::~SDLImage() {
  IMG_Quit();
}

SDLTexture SDLImage::load(const SDLRenderer& renderer, const std::string& file) {
  SDL_Texture* texture = IMG_LoadTexture(renderer.get(), file.c_str());
  if (texture == nullptr) {
    throw SDLException("Failed loading texture from image: " + file);
  }
  return SDLTexture(texture);
}

SDLSurface SDLImage::load(const std::string& file) {
  SDL_Surface* surface = IMG_Load(file.c_str());
  if (surface == nullptr) {
    throw SDLException("Failed loading surface from image: " + file);
  }
  return SDLSurface(surface);
}
