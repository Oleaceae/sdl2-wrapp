#ifndef SDL_TTF_WRAPPER_H
#define SDL_TTF_WRAPPER_H

#include "sdl.h"
#include <SDL2/SDL_ttf.h>
#include <string>

namespace sdlwrapp {

struct Vector2D {
    int x{};
    int y{};
};

class TTF {
public:
  explicit TTF(const std::string& fontPath, int size = 16);
  ~TTF();
  TTF(const TTF& other) = delete;
  TTF(TTF&& other) noexcept;

  TTF& operator=(const TTF& other) = delete;
  TTF& operator=(TTF&& other) noexcept;

  [[nodiscard]] SDLSurface renderTextBlended(const std::string& text, const SDLColor& color = {128, 128, 128}) const;
  [[nodiscard]] SDLSurface renderUnicodeBlended(const std::u16string& text,
                                                const SDLColor& color = {128, 128, 128}) const;

  [[nodiscard]] std::uint32_t getSize() const noexcept;
  [[nodiscard]] Vector2D getTextSize(const std::string& text) const;
  [[nodiscard]] Vector2D getUnicodeSize(const std::u16string& text) const;

private:
  std::string fontLocation{};
  std::uint32_t size = 16;
  TTF_Font* font{nullptr};
};

} // namespace sdlwrapp
#endif
