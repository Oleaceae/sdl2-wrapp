#include "sdlttf.h"
#include <utility>

using namespace sdlwrapp;

TTF::TTF(const std::string& fontPath, const int size) : size(size) {
  if (TTF_Init() < 0) {
    throw SDLException();
  }
  this->font = TTF_OpenFont(fontPath.c_str(), size);
  if (this->font == nullptr) {
    throw SDLException();
  }
}

TTF::~TTF() {
  TTF_CloseFont(this->font);
}

SDLSurface TTF::renderTextBlended(const std::string& text, const SDLColor& color) const {
  SDL_Surface* surface = TTF_RenderText_Blended(this->font, text.c_str(), color.get());
  if (surface == nullptr) {
    throw SDLException();
  }
  return SDLSurface(surface);
}

SDLSurface TTF::renderUnicodeBlended(const std::u16string& text, const SDLColor& color) const {
  SDL_Surface* surface =
      TTF_RenderUNICODE_Blended(this->font, reinterpret_cast<const Uint16*>(text.c_str()), color.get());
  if (surface == nullptr) {
    throw SDLException();
  }
  return SDLSurface(surface);
}

std::uint32_t TTF::getSize() const noexcept {
  return this->size;
}

Vector2D TTF::getTextSize(const std::string& text) const {
  int w{}, h{};
  if (TTF_SizeText(this->font, text.c_str(), &w, &h) < 0) {
    throw SDLException();
  }
  return {w, h};
}

Vector2D TTF::getUnicodeSize(const std::u16string& text) const {
  int w{}, h{};
  if (TTF_SizeUNICODE(this->font, reinterpret_cast<const Uint16*>(text.c_str()), &w, &h) < 0) {
    throw SDLException();
  }
  return {w, h};
}

TTF::TTF(TTF&& other) noexcept {
  std::swap(other.font, this->font);
}

TTF& TTF::operator=(TTF&& other) noexcept {
  if (this != &other) {
    std::swap(other.font, this->font);

    this->fontLocation = other.fontLocation;
    this->size = other.size;
  }
  return *this;
}
