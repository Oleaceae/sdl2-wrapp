#ifndef SDL_WRAPPER_H
#define SDL_WRAPPER_H

#include <SDL2/SDL.h>
#include <cstdint>
#include <exception>
#include <set>
#include <string>

// Remove SDL2's #define main
#undef main

namespace sdlwrapp {

class SDLException final : std::exception {
public:
  SDLException() = default;
  explicit SDLException(const std::string& setMessage);
  [[nodiscard]] const char* what() const noexcept override;
};

class SDL {
public:
  SDL();
  ~SDL();

private:
};

struct SDLFRect {
  constexpr SDLFRect() = default;
  constexpr SDLFRect(const float x, const float y, const float width, const float height) noexcept
      : rect({x, y, width, height}) {
  }

  SDL_FRect rect{};
};

struct SDLRect {
  constexpr SDLRect() = default;
  constexpr SDLRect(const int x, const int y, const int width, const int height) noexcept
      : rect({x, y, width, height}) {
  }

  SDL_Rect rect{};
};

class SDLColor {
public:
  constexpr SDLColor() = default;
  constexpr SDLColor(const std::uint8_t red, const std::uint8_t green, const std::uint8_t blue,
                     const std::uint8_t alpha = 255) noexcept
      : color({red, green, blue, alpha}) {
  }

  [[nodiscard]] SDL_Color get() const noexcept;

private:
  SDL_Color color = {0, 0, 0, 255};
};

enum class BlendMode {
  none = SDL_BLENDMODE_NONE,
  blend = SDL_BLENDMODE_BLEND,
  add = SDL_BLENDMODE_ADD,
  mod = SDL_BLENDMODE_MOD,
  mul = SDL_BLENDMODE_MUL
};

class SDLSurface {
public:
  SDLSurface() noexcept = default;
  explicit SDLSurface(SDL_Surface* surface) noexcept : surface(surface){};
  SDLSurface(SDLSurface&& source) noexcept;
  SDLSurface(const SDLSurface&) = delete;
  ~SDLSurface() noexcept;

  SDLSurface& operator=(const SDLSurface& other) = delete;
  SDLSurface& operator=(SDLSurface&& other) noexcept;

  [[nodiscard]] SDL_Surface* get() const noexcept;
  [[nodiscard]] bool isNull() const noexcept;

  void blit(const SDLSurface& destination) const;
  void blit(const SDLSurface& destination, const SDLRect& destinationRect) const;
  void blit(const SDLRect& sourceRect, const SDLSurface& destination, const SDLRect& destinationRect) const;

  void setBlendMode(BlendMode mode) const;

  static SDLSurface loadBMP(const std::string& filepath);
  static SDLSurface createRGBSurface(std::uint32_t flags, int width, int height, int depth, std::uint32_t Rmask,
                                     std::uint32_t Gmask, std::uint32_t Bmask, std::uint32_t Amask);
  static SDLSurface createTransparentRGBSurface(int width, int height);

private:
  SDL_Surface* surface = nullptr;
};

enum class WindowFlag {
  fullscreen = SDL_WINDOW_FULLSCREEN,
  fullscreenDesktop = SDL_WINDOW_FULLSCREEN_DESKTOP,
  borderless = SDL_WINDOW_FULLSCREEN_DESKTOP,
  resizable = SDL_WINDOW_RESIZABLE,
  windowed = 0
};

class SDLWindow {
public:
  SDLWindow() noexcept = default;
  explicit SDLWindow(SDL_Window* window, const WindowFlag winMode) noexcept : windowMode(winMode), window(window){};
  SDLWindow(const SDLWindow&) = delete;
  SDLWindow(SDLWindow&& source) noexcept;
  ~SDLWindow() noexcept;

  SDLWindow& operator=(SDLWindow&& other) noexcept;

  [[nodiscard]] SDL_Window* get() const noexcept;
  [[nodiscard]] SDLSurface getSurface() const;
  [[nodiscard]] int getHeight() const;
  [[nodiscard]] int getWidth() const;
  [[nodiscard]] bool isNull() const noexcept;

  void setWindowResizable(bool resizable) const noexcept;
  void setWindowIcon(const SDLSurface& icon) const noexcept;
  void updateWindowSurface() const;

  static SDLWindow createWindow(const std::string& title, int x, int y, int width, int height,
                                const std::set<WindowFlag>& flags = {});

  void setWindowTitle(const std::string& title) const noexcept;
  void setWindowMode(WindowFlag mode);
  void switchWindowMode();

  WindowFlag windowMode{};

private:
  SDL_Window* window{nullptr};
};

enum class RendererFlag {
  software = SDL_RENDERER_SOFTWARE,
  accelerated = SDL_RENDERER_ACCELERATED,
  presentvsync = SDL_RENDERER_PRESENTVSYNC,
  targettexture = SDL_RENDERER_TARGETTEXTURE
};

class SDLTexture;

class SDLRenderer {
public:
  SDLRenderer() noexcept = default;
  explicit SDLRenderer(SDL_Renderer* renderer) noexcept : renderer(renderer){};
  explicit SDLRenderer(const SDLWindow& window, int gpuIndex = firstSupportingGPU,
                       const std::set<RendererFlag>& flags = {RendererFlag::accelerated});
  SDLRenderer(const SDLRenderer& other) = delete;
  SDLRenderer(SDLRenderer&& other) noexcept;
  ~SDLRenderer() noexcept;

  static SDLRenderer softwareRenderer(const SDLSurface& target);

  SDLRenderer& operator=(const SDLRenderer& other) = delete;
  SDLRenderer& operator=(SDLRenderer&& other) noexcept;

  void setRenderDrawColor(const SDLColor& color) const;
  void setRenderTarget(const SDLTexture& texture) const;
  void resetRenderTarget() const;
  void drawRect(const SDLRect& rectangle) const;
  void drawRectColor(const SDLRect& rectangle, const SDLColor& color) const;
  void fillRect(const SDLRect& rectangle) const;
  void fillRectColor(const SDLRect& rectangle, const SDLColor& color) const;
  void clear() const;
  void present() const;

  [[nodiscard]] SDL_Renderer* get() const noexcept;
  void reset() noexcept;

private:
  SDL_Renderer* renderer = nullptr;

  static constexpr int firstSupportingGPU = -1;
};

class SDLTexture {
public:
  SDLTexture() noexcept = default;
  explicit SDLTexture(SDL_Texture* texture) noexcept : texture(texture){};
  SDLTexture(const SDLRenderer&, const SDLSurface&);
  ~SDLTexture() noexcept;
  SDLTexture(const SDLTexture& other) = delete;
  SDLTexture(SDLTexture&& other) noexcept;

  SDLTexture& operator=(const SDLTexture& other) = delete;
  SDLTexture& operator=(SDLTexture&& other) noexcept;
  bool operator==(const SDLTexture& other) const noexcept;
  bool operator==(const SDL_Texture* other) const noexcept;

  // Pixel precision rendering
  void render(const SDLRenderer* renderer, const SDLRect* destRect = nullptr, const SDLRect* srcRect = nullptr,
              double angle = 0.0, const SDL_Point* center = nullptr, SDL_RendererFlip flip = SDL_FLIP_NONE) const;

  // Subpixel precision rendering
  void render(const SDLRenderer* renderer, const SDLFRect* destRect = nullptr, const SDLRect* srcRect = nullptr,
              double angle = 0.0, const SDL_FPoint* center = nullptr, SDL_RendererFlip flip = SDL_FLIP_NONE) const;

  void setBlendMode(BlendMode mode) const;

  [[nodiscard]] SDL_Texture* get() const noexcept;

  static SDLTexture blankRenderingTarget(const SDLRenderer&, int w, int h);

private:
  SDL_Texture* texture = nullptr;
};

} // namespace sdlwrapp

#endif
