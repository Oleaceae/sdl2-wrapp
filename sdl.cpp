#include "sdl.h"
#include <utility>

using namespace sdlwrapp;

SDLException::SDLException(const std::string& setMessage) {
  SDL_SetError("%s", setMessage.c_str());
}

const char* SDLException::what() const noexcept {
  return SDL_GetError();
}

SDL::SDL() {
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
    throw SDLException();
  }
}

SDL::~SDL() {
  SDL_Quit();
}

SDL_Color SDLColor::get() const noexcept {
  return this->color;
}

SDLSurface::SDLSurface(SDLSurface&& source) noexcept {
  std::swap(this->surface, source.surface);
}

SDLSurface::~SDLSurface() noexcept {
  SDL_FreeSurface(this->surface);
}

SDL_Surface* SDLSurface::get() const noexcept {
  return this->surface;
}

bool SDLSurface::isNull() const noexcept {
  return this->surface == nullptr;
}

void SDLSurface::blit(const SDLSurface& destination) const {
  if (SDL_BlitSurface(this->surface, nullptr, destination.get(), nullptr) < 0) {
    throw SDLException();
  }
}

void SDLSurface::blit(const SDLSurface& destination, const SDLRect& destinationRect) const {
  // Why const has to be removed manually:
  // https://discourse.libsdl.org/t/lack-of-const-correctness-in-sdl-blitsurface/4636/5 Safe since libsdl 1.2
  if (SDL_BlitSurface(this->surface, nullptr, destination.get(), const_cast<SDL_Rect*>(&destinationRect.rect)) < 0) {
    throw SDLException();
  }
}

void SDLSurface::blit(const SDLRect& sourceRect, const SDLSurface& destination, const SDLRect& destinationRect) const {
  if (SDL_BlitSurface(this->surface, &sourceRect.rect, destination.get(),
                      const_cast<SDL_Rect*>(&destinationRect.rect)) < 0) {
    throw SDLException();
  }
}

void SDLSurface::setBlendMode(BlendMode mode) const {
  if (SDL_SetSurfaceBlendMode(this->surface, static_cast<SDL_BlendMode>(mode)) < 0) {
    throw SDLException();
  }
}

SDLSurface SDLSurface::loadBMP(const std::string& filepath) {
  SDLSurface surface = SDLSurface(SDL_LoadBMP(filepath.c_str()));
  if (surface.isNull()) {
    throw SDLException();
  }
  return surface;
}

SDLSurface SDLSurface::createRGBSurface(const std::uint32_t flags, const int width, const int height, const int depth,
                                        const std::uint32_t Rmask, const std::uint32_t Gmask, const std::uint32_t Bmask,
                                        const std::uint32_t Amask) {
  const auto surface = SDL_CreateRGBSurface(flags, width, height, depth, Rmask, Gmask, Bmask, Amask);
  if (surface == nullptr) {
    throw SDLException();
  }
  return SDLSurface(surface);
}

SDLSurface SDLSurface::createTransparentRGBSurface(const int width, const int height) {
  return createRGBSurface(0, width, height, 32, 0x000000FF, 0x0000FF00, 0x00FF0000, 0xFF000000);
}

SDLSurface& SDLSurface::operator=(SDLSurface&& other) noexcept {
  if (this != &other) {
    std::swap(this->surface, other.surface);
  }
  return *this;
}

SDLWindow::SDLWindow(SDLWindow&& source) noexcept {
  std::swap(this->window, source.window);
  this->windowMode = source.windowMode;
}

SDLWindow::~SDLWindow() noexcept {
  SDL_DestroyWindow(this->window);
}

SDLWindow& SDLWindow::operator=(SDLWindow&& other) noexcept {
  if (this != &other) {
    std::swap(this->window, other.window);
  }
  return *this;
}

SDL_Window* SDLWindow::get() const noexcept {
  return this->window;
}

SDLSurface SDLWindow::getSurface() const {
  SDL_Surface* surface = SDL_GetWindowSurface(this->window);
  if (surface == nullptr) {
    throw SDLException();
  }
  return SDLSurface(surface);
}

int SDLWindow::getHeight() const {
  int y;
  SDL_GetWindowSize(this->window, nullptr, &y);
  return y;
}

int SDLWindow::getWidth() const {
  int x;
  SDL_GetWindowSize(this->window, &x, nullptr);
  return x;
}

bool SDLWindow::isNull() const noexcept {
  return this->window == nullptr;
}

void SDLWindow::setWindowResizable(const bool resizable) const noexcept {
  SDL_SetWindowResizable(this->window, resizable ? SDL_TRUE : SDL_FALSE);
}

void SDLWindow::setWindowIcon(const SDLSurface& icon) const noexcept {
  SDL_SetWindowIcon(this->window, icon.get());
}

SDLWindow SDLWindow::createWindow(const std::string& title, const int x, const int y, const int width, const int height,
                                  const std::set<WindowFlag>& flags) {
  std::uint32_t flag = 0;
  for (auto f : flags) {
    flag = flag | static_cast<std::uint32_t>(f);
  }
  SDL_Window* window = SDL_CreateWindow(title.c_str(), x, y, width, height, flag);
  if (window == nullptr) {
    throw SDLException();
  }

  auto initialMode = WindowFlag::windowed;
  if (flag & static_cast<std::uint32_t>(WindowFlag::fullscreen)) {
    initialMode = WindowFlag::fullscreen;
  } else if (flag & static_cast<std::uint32_t>(WindowFlag::fullscreenDesktop)) {
    initialMode = WindowFlag::fullscreenDesktop;
  }
  return SDLWindow(window, initialMode);
}

void SDLWindow::updateWindowSurface() const {
  if (SDL_UpdateWindowSurface(this->window) < 0) {
    throw SDLException();
  }
}

void SDLWindow::setWindowTitle(const std::string& title) const noexcept {
  if (this->window != nullptr) {
    SDL_SetWindowTitle(this->window, title.c_str());
  }
}

void SDLWindow::setWindowMode(WindowFlag mode) {
  if (mode != WindowFlag::fullscreen && mode != WindowFlag::fullscreenDesktop && mode != WindowFlag::windowed) {
    mode = WindowFlag::windowed;
  }
  if (SDL_SetWindowFullscreen(this->window, static_cast<std::uint32_t>(mode)) < 0) {
    throw SDLException();
  }
  this->windowMode = mode;
}

void SDLWindow::switchWindowMode() {
  switch (this->windowMode) {
  case WindowFlag::fullscreenDesktop:
    this->setWindowMode(WindowFlag::windowed);
    this->windowMode = WindowFlag::windowed;
    break;
  case WindowFlag::windowed:
    this->setWindowMode(WindowFlag::fullscreenDesktop);
    this->windowMode = WindowFlag::fullscreenDesktop;
    break;
  default:
    break;
  }
}

SDLRenderer::SDLRenderer(const SDLWindow& window, const int gpuIndex, const std::set<RendererFlag>& flags) {
  std::uint32_t flag = 0;
  for (auto f : flags) {
    flag = flag | static_cast<std::uint32_t>(f);
  }
  this->renderer = SDL_CreateRenderer(window.get(), gpuIndex, flag);
  if (this->renderer == nullptr) {
    throw SDLException();
  }
  if (SDL_SetRenderDrawBlendMode(this->renderer, SDL_BLENDMODE_BLEND) < 0) {
    throw SDLException();
  }
}

SDLRenderer::SDLRenderer(SDLRenderer&& other) noexcept {
  std::swap(this->renderer, other.renderer);
}

SDLRenderer::~SDLRenderer() noexcept {
  SDL_DestroyRenderer(this->renderer);
}

SDLRenderer SDLRenderer::softwareRenderer(const SDLSurface& target) {
  SDL_Renderer* renderer = SDL_CreateSoftwareRenderer(target.get());
  if (renderer == nullptr) {
    throw SDLException();
  }
  return SDLRenderer(renderer);
}

SDLRenderer& SDLRenderer::operator=(SDLRenderer&& other) noexcept {
  if (this != &other) {
    std::swap(this->renderer, other.renderer);
  }
  return *this;
}

void SDLRenderer::setRenderDrawColor(const SDLColor& color) const {
  auto [r, g, b, a] = color.get();
  if (SDL_SetRenderDrawColor(this->get(), r, g, b, a) < 0) {
    throw SDLException();
  }
}

void SDLRenderer::setRenderTarget(const SDLTexture& texture) const {
  if (SDL_SetRenderTarget(this->renderer, texture.get()) < 0) {
    throw SDLException();
  }
}

void SDLRenderer::resetRenderTarget() const {
  if (SDL_SetRenderTarget(this->renderer, nullptr) < 0) {
    throw SDLException();
  }
}

void SDLRenderer::drawRect(const SDLRect& rectangle) const {
  if (SDL_RenderDrawRect(this->get(), &rectangle.rect) < 0) {
    throw SDLException();
  }
}

void SDLRenderer::drawRectColor(const SDLRect& rectangle, const SDLColor& color) const {
  this->setRenderDrawColor(color);
  this->drawRect(rectangle);
}

void SDLRenderer::fillRect(const SDLRect& rectangle) const {
  if (SDL_RenderFillRect(this->get(), &rectangle.rect) < 0) {
    throw SDLException();
  }
}

void SDLRenderer::fillRectColor(const SDLRect& rectangle, const SDLColor& color) const {
  this->setRenderDrawColor(color);
  this->fillRect(rectangle);
}

SDL_Renderer* SDLRenderer::get() const noexcept {
  return this->renderer;
}

void SDLRenderer::reset() noexcept {
  this->renderer = nullptr;
}
void SDLRenderer::clear() const {
  if (SDL_SetRenderDrawColor(this->renderer, 0x0F, 0x0F, 0x0F, 0x00) < 0) {
    throw SDLException();
  }
  if (SDL_RenderClear(this->renderer) < 0) {
    throw SDLException();
  }
}

void SDLRenderer::present() const {
  SDL_RenderPresent(this->renderer);
}

SDLTexture::SDLTexture(const SDLRenderer& renderer, const SDLSurface& surface) {
  this->texture = SDL_CreateTextureFromSurface(renderer.get(), surface.get());
  if (this->texture == nullptr) {
    throw SDLException();
  }
}

SDLTexture::~SDLTexture() noexcept {
  if (this->texture != nullptr) {
    SDL_DestroyTexture(this->texture);
  }
}

SDLTexture::SDLTexture(SDLTexture&& other) noexcept {
  std::swap(this->texture, other.texture);
}

SDLTexture& SDLTexture::operator=(SDLTexture&& other) noexcept {
  if (this != &other) {
    std::swap(this->texture, other.texture);
  }
  return *this;
}

bool SDLTexture::operator==(const SDLTexture& other) const noexcept {
  return this->texture == other.texture;
}

bool SDLTexture::operator==(const SDL_Texture* other) const noexcept {
  return this->texture == other;
}

void SDLTexture::render(const SDLRenderer* renderer, const SDLRect* destRect, const SDLRect* srcRect,
                        const double angle, const SDL_Point* center, const SDL_RendererFlip flip) const {
  if (SDL_RenderCopyEx(renderer->get(), this->texture, &srcRect->rect, &destRect->rect, angle, center, flip) < 0) {
    throw SDLException();
  }
}

void SDLTexture::render(const SDLRenderer* renderer, const SDLFRect* destRect, const SDLRect* srcRect,
                        const double angle, const SDL_FPoint* center, const SDL_RendererFlip flip) const {
  if (SDL_RenderCopyExF(renderer->get(), this->texture, &srcRect->rect, &destRect->rect, angle, center, flip) < 0) {
    throw SDLException();
  }
}

void SDLTexture::setBlendMode(BlendMode mode) const {
  if (SDL_SetTextureBlendMode(this->texture, static_cast<SDL_BlendMode>(mode)) < 0) {
    throw SDLException();
  }
}

SDL_Texture* SDLTexture::get() const noexcept {
  return this->texture;
}

SDLTexture SDLTexture::blankRenderingTarget(const SDLRenderer& renderer, const int w, const int h) {
  auto text = SDLTexture(SDL_CreateTexture(renderer.get(), SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, w, h));
  if (text.get() == nullptr) {
    throw SDLException();
  }
  text.setBlendMode(BlendMode::blend);
  return text;
}
