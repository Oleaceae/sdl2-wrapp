#include "sdlmixer.h"

#include <utility>

#include "sdl.h"

using namespace sdlwrapp;

SDLMixer::SDLMixer() {
  if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) == -1) {
    throw SDLException();
  }
}

SDLMixer::~SDLMixer() {
  Mix_HaltChannel(-1);
  Mix_CloseAudio();
}

void SDLMixer::setChannelCount(const int channels) noexcept {
  Mix_AllocateChannels(channels);
}

void SDLMixer::setChannelFinishedCallback(void(SDLCALL* channelFinished)(int channel)) noexcept {
  Mix_ChannelFinished(channelFinished);
}

MixChunk::MixChunk(const std::string& filePath) {
  this->chunk = Mix_LoadWAV(filePath.c_str());
}

MixChunk::~MixChunk() {
  Mix_FreeChunk(this->chunk);
}

MixChunk::MixChunk(MixChunk&& other) noexcept {
  std::swap(other.chunk, this->chunk);
}

MixChunk& MixChunk::operator=(MixChunk&& other) noexcept {
  if (this != &other) {
    std::swap(other.chunk, this->chunk);
  }
  return *this;
}

int MixChunk::play(const int channel, const int loops) const {
  return Mix_PlayChannel(channel, this->chunk, loops);
}

bool MixChunk::isNull() const noexcept {
  return this->chunk == nullptr;
}

MixMusic::MixMusic(const std::string& filePath) {
  this->music = Mix_LoadMUS(filePath.c_str());
}

MixMusic::~MixMusic() {
  Mix_FreeMusic(this->music);
}

MixMusic::MixMusic(MixMusic&& other) noexcept {
  this->music = other.music;
  std::swap(other.music, this->music);
}

MixMusic& MixMusic::operator=(MixMusic&& other) noexcept {
  if (this != &other) {
    this->music = other.music;
    other.music = nullptr;
  }
  return *this;
}

int MixMusic::play(const int loops) const {
  return Mix_PlayMusic(this->music, loops);
}

void MixMusic::pause() {
  Mix_PauseMusic();
}

void MixMusic::resume() {
  Mix_ResumeMusic();
}

void MixMusic::halt() {
  Mix_HaltMusic();
}

int MixMusic::setVolume(const int volume) {
  return Mix_VolumeMusic(volume);
}

bool MixMusic::isNull() const noexcept {
  return (this->music == nullptr);
}
